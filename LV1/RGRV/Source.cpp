#include "pch.h"
#include <iostream>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <time.h>

using namespace std;
using namespace cv;

int window_width = 440;
int window_height = 440;
int pt_shift = 20;

Mat img;
Mat img_copy;

void paintTriangle(Point2f first_pt, Point2f second_pt, Point2f third_pt);

void paintNeighbours(int first_edge_id, int second_edge_id, int third_edge_id, Subdiv2D* subdiv);

bool if_point_in_triangle(Point T, int edge_id, int next_around_org_edge_id, Subdiv2D* subdiv);

static void onMouse(int event, int x, int y, int flags, void* param);

void draw_delaunay(vector<int> edgesList_ids, Subdiv2D* subdiv);

void get_edges_ids(Subdiv2D* subdiv, vector<int>* edgesList_ids);

void insert_points_in_subdiv(Subdiv2D* subdiv, Rect* rect);

int main()
{
	srand(unsigned(time(NULL)));

	img.create(window_width, window_height, CV_8UC3);
	img.setTo(255);

	Rect rect(0, 0, window_width, window_height);
	Subdiv2D subdiv(rect);
	insert_points_in_subdiv(&subdiv, &rect);

	vector<int> edgesList_ids;
	get_edges_ids(&subdiv, &edgesList_ids);

	draw_delaunay(edgesList_ids, &subdiv);

	namedWindow("LV2");
	imshow("LV2", img);

	setMouseCallback("LV2", onMouse, (void*)(&subdiv));
	waitKey(0);

	return 0;
}

void insert_points_in_subdiv(Subdiv2D* subdiv, Rect* rect)
{
	subdiv->insert(Point2f(20, 20));
	subdiv->insert(Point2f(20, 420));
	subdiv->insert(Point2f(420, 20));
	subdiv->insert(Point2f(420, 420));

	for (int i = 0; i < 16; i++)
	{
		subdiv->insert(Point2f((int)(rand() % (rect->width - pt_shift * 2) + pt_shift),
			(int)(rand() % (rect->height - pt_shift * 2) + pt_shift)));
	}
}

void get_edges_ids(Subdiv2D* subdiv, vector<int>* edgesList_ids)
{
	for (int i = 4; i < 24; i++)
	{
		int first_edge_id;
		subdiv->getVertex(i, &first_edge_id);

		edgesList_ids->push_back(first_edge_id);

		int temp_edge = first_edge_id;
		while (1)
		{
			int next_edge_id = subdiv->getEdge(temp_edge, Subdiv2D::NEXT_AROUND_ORG);
			if (first_edge_id == next_edge_id)
				break;
			else
			{
				edgesList_ids->push_back(next_edge_id);
				temp_edge = next_edge_id;
			}
		}
		cout << (i - 3) << ". " << subdiv->getVertex(i) << endl;
	}
	cout << "Num of edges in list: " << edgesList_ids->size() << endl;
}

void draw_delaunay(vector<int> edgesList_ids, Subdiv2D* subdiv)
{
	for (auto i = edgesList_ids.begin(); i != edgesList_ids.end(); ++i)
	{

		Point2f pt_dst, pt_org;

		if (subdiv->edgeDst(*i, &pt_dst) > subdiv->edgeOrg(*i, &pt_org))
		{
			line(img, pt_org, pt_dst, Scalar(0, 0, 0), 1, CV_AA, 0);
		}
	}
}

static void onMouse(int event, int x, int y, int flags, void* param)
{
	if (event == CV_EVENT_LBUTTONDOWN)
	{
		Point clicked_pt(x, y);
		Subdiv2D* subdiv = (Subdiv2D*)param;

		if (clicked_pt.x >= 20 && clicked_pt.x <= 420 && clicked_pt.y >= 20 && clicked_pt.y <= 420)
		{

			int nearest_pt_id = subdiv->findNearest(clicked_pt);
			int first_edge_id;
			subdiv->getVertex(nearest_pt_id, &first_edge_id);

			int temp_edge = first_edge_id;
			while (1)
			{
				int next_around_org_edge_id = subdiv->getEdge(temp_edge, Subdiv2D::NEXT_AROUND_ORG);
				if (if_point_in_triangle(clicked_pt, temp_edge, next_around_org_edge_id, subdiv))
				{
					Point2f pt1, pt2, pt3;
					subdiv->edgeOrg(temp_edge, &pt1);
					subdiv->edgeDst(temp_edge, &pt2);
					subdiv->edgeDst(next_around_org_edge_id, &pt3);

					img_copy = img.clone();

					paintNeighbours(subdiv->rotateEdge(temp_edge, 2),
						next_around_org_edge_id,
						subdiv->rotateEdge(subdiv->getEdge(temp_edge, Subdiv2D::NEXT_AROUND_LEFT), 2),
						subdiv);
					paintTriangle(pt1, pt2, pt3);

					imshow("LV2", img_copy);

					break;
				}
				else
				{
					temp_edge = next_around_org_edge_id;
					if (temp_edge == first_edge_id)
						break;
				}
			}
		}
	}
}

bool if_point_in_triangle(Point T, int edge_id, int next_around_org_edge_id, Subdiv2D* subdiv)
{
	Point2f A, B, C;
	subdiv->edgeOrg(edge_id, &A);
	subdiv->edgeDst(edge_id, &B);
	subdiv->edgeDst(next_around_org_edge_id, &C);

	bool A_in_range = A.x >= 20 && A.x <= 420 && A.y >= 20 && A.y <= 420;
	bool B_in_range = B.x >= 20 && B.x <= 420 && B.y >= 20 && B.y <= 420;
	bool C_in_range = C.x >= 20 && C.x <= 420 && C.y >= 20 && C.y <= 420;
	bool T_in_range = C.x >= 20 && C.x <= 420 && C.y >= 20 && C.y <= 420;

	if (T_in_range)
	{
		if (A_in_range && B_in_range && C_in_range)
		{
			//Barycentric coordinates
			float mi = ((B.x - A.x) * (T.y - A.y) - (B.y - A.y) * (T.x - A.x)) / ((C.y - A.y) * (B.x - A.x) - (B.y - A.y) * (C.x - A.x));
			float lambda = (T.x - A.x - mi * (C.x - A.x)) / (B.x - A.x);
			if (lambda >= 0.0 && mi >= 0.0 && (lambda + mi) <= 1.0)
			{
				return true;
			}
		}
	}
	return false;
}

void paintNeighbours(int first_edge_id, int second_edge_id, int third_edge_id, Subdiv2D* subdiv)
{
	Point2f first_edge_next_around_left_pt, second_edge_next_around_left_pt, third_edge_next_around_left_pt;

	subdiv->edgeDst(subdiv->getEdge(first_edge_id, Subdiv2D::NEXT_AROUND_LEFT), &first_edge_next_around_left_pt);
	subdiv->edgeDst(subdiv->getEdge(second_edge_id, Subdiv2D::NEXT_AROUND_LEFT), &second_edge_next_around_left_pt);
	subdiv->edgeDst(subdiv->getEdge(third_edge_id, Subdiv2D::NEXT_AROUND_LEFT), &third_edge_next_around_left_pt);

	if (first_edge_next_around_left_pt.x >= 20 &&
		first_edge_next_around_left_pt.x <= 420 &&
		first_edge_next_around_left_pt.y >= 20 &&
		first_edge_next_around_left_pt.y <= 420)
	{
		Point2f pt1, pt2;
		subdiv->edgeOrg(first_edge_id, &pt1);
		subdiv->edgeDst(first_edge_id, &pt2);
		vector<Point> pts;
		pts.push_back(pt1);
		pts.push_back(pt2);
		pts.push_back(first_edge_next_around_left_pt);
		fillConvexPoly(img_copy, pts, Scalar(255, 0, 0), CV_AA, 0);
	}

	if (second_edge_next_around_left_pt.x >= 20 &&
		second_edge_next_around_left_pt.x <= 420 &&
		second_edge_next_around_left_pt.y >= 20 &&
		second_edge_next_around_left_pt.y <= 420)
	{
		Point2f pt1, pt2;
		subdiv->edgeOrg(second_edge_id, &pt1);
		subdiv->edgeDst(second_edge_id, &pt2);
		vector<Point> pts;
		pts.push_back(pt1);
		pts.push_back(pt2);
		pts.push_back(second_edge_next_around_left_pt);
		fillConvexPoly(img_copy, pts, Scalar(255, 0, 0), CV_AA, 0);
	}
	if (third_edge_next_around_left_pt.x >= 20 &&
		third_edge_next_around_left_pt.x <= 420 &&
		third_edge_next_around_left_pt.y >= 20 &&
		third_edge_next_around_left_pt.y <= 420)
	{
		Point2f pt1, pt2;
		subdiv->edgeOrg(third_edge_id, &pt1);
		subdiv->edgeDst(third_edge_id, &pt2);
		vector<Point> pts;
		pts.push_back(pt1);
		pts.push_back(pt2);
		pts.push_back(third_edge_next_around_left_pt);
		fillConvexPoly(img_copy, pts, Scalar(255, 0, 0), CV_AA, 0);
	}
}

void paintTriangle(Point2f first_pt, Point2f second_pt, Point2f third_pt)
{
	vector<Point> pts;
	pts.push_back(first_pt);
	pts.push_back(second_pt);
	pts.push_back(third_pt);

	fillConvexPoly(img_copy, pts, Scalar(0, 0, 255), CV_AA, 0);
}