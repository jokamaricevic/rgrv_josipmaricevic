// RG_LV1.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <conio.h>
#include <vector>
#include <math.h>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>

#include <opencv2/core.hpp>
//#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>



using namespace cv;
using namespace std;


static void Mouse_click(int event, int x, int y, int flags, void* param);

struct User_data {
	int num_clicks = 0;
	Mat img_pts = Mat::zeros(4, 2, CV_32F);
	Mat obj_pts = Mat::zeros(4, 3, CV_32F);
};



int main(int argc, char* argv[])
{



	//Start video capture
	VideoCapture cap(0); // cap(0) -> open the default/inbuilt camera, ako imamo neku naknadno dodanu kameru stavljamo 1 
	Mat cameraMatrix, distCoeffs;

	int flag;
	cout << "pritisinit 1 za kalibraciju kamere ili 2 za koristenje vec kalibriranih parametara: ";
	cin >> flag;

	if (flag == 1) {
		int m_MaxNImages;	//max number of images taken  (5)
		int m_BoardWidth;	//board width (ie the max number of corners that can be found along the chessboard length) (8)
		int m_BoardHeight;	//board height(ie the max number of corners that can be found along the chessboard height) (6)
		float m_SquareLength;	//the length of one of the squares on the chessboard (in mm)  (36)


		cout << "Max number of images to take:";
		cin >> m_MaxNImages;
		cout << "Board width (ie the max number of corners that can be found along the chessboard width):";
		cin >> m_BoardWidth;
		cout << "Board height(ie the max number of corners that can be found along the chessboard height):";
		cin >> m_BoardHeight;
		cout << "Length of one of the squares on the chessboard (in mm):";
		cin >> m_SquareLength;

		Size imageSize;

		vector<Point2f> corners;
		vector<vector<Point2f> > imagePoints;

		Size board_sz = Size(m_BoardWidth, m_BoardHeight);

		int successes = 0;
		const int ESC_KEY = 27;


		if (cap.isOpened())  // check if we succeeded
		{

			int c = 0;
			bool found;

			cout << "\n pritisni P za pokretanje\n" << endl;

			namedWindow("Current view", 1);
			for (;;)
			{
				c = waitKey(15);

				// get a new frame from camera
				Mat frame, imgclone, imggray;

				cap >> frame;

				//display the frame 
				imshow("Current view", frame);

				imageSize = frame.size();
				//Use frame to determine chessboard corners:
				if (c == 'p')
				{
					if (successes < m_MaxNImages)
					{
						imgclone = frame.clone();
						imggray.create(imgclone.size(), CV_8UC1);

						//Find chessboard corners:
						found = findChessboardCorners(imgclone, board_sz, corners, CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE);

						if (found)
						{
							// improve the found corners' coordinate accuracy for chessboard
							Mat viewGray;
							cvtColor(imgclone, imggray, COLOR_BGR2GRAY);
							cornerSubPix(imggray, corners, Size(11, 11),
								Size(-1, -1), TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));

							// Draw the corners.
							drawChessboardCorners(imgclone, board_sz, Mat(corners), found);

							imagePoints.push_back(corners);

							successes++;
						}
						imshow("Calibration", imgclone);
					}
					else
						break;
				}

				if (c == ESC_KEY) break;
			}
		}
		// the camera will be deinitialized automatically in VideoCapture destructor
		//END COLLECTION WHILE LOOP.

		//close all windows
		destroyAllWindows();

		if (successes == m_MaxNImages)
		{
			//PERFORM CALIBRATION
			vector<Mat> rvecs, tvecs;
			vector<float> reprojErrs;
			double totalAvgErr = 0;

			cameraMatrix = Mat::eye(3, 3, CV_64F);
			distCoeffs = Mat::zeros(8, 1, CV_64F);

			vector<vector<Point3f> > objectPoints(1);

			//Compute board_corners
			for (int i = 0; i < board_sz.height; ++i)
				for (int j = 0; j < board_sz.width; ++j)
					objectPoints[0].push_back(Point3f(j * m_SquareLength, i * m_SquareLength, 0));


			objectPoints.resize(imagePoints.size(), objectPoints[0]);

			//Find intrinsic and extrinsic camera parameters
			double rms;
			rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, 0);

			cout << "Re-projection error reported by calibrateCamera: " << rms << endl;

			bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

			//Calculate average reprojection error
			//totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

			cout << (ok ? "Calibration succeeded" : "Calibration failed");



			//SAVE PARAMS
			/*FileStorage fs("cameraparams.xml", FileStorage::WRITE);
			fs << "camera_matrix" << cameraMatrix;
			fs << "distortion_coefficients" << distCoeffs;
			fs.release();*/



			// LOAD THESE MATRICES BACK IN AND DISPLAY THE UNDISTORTED IMAGE
			Mat cameraMatrixLoaded, distCoeffsLoaded;
			FileStorage fsL("cameraparams.xml", FileStorage::READ);
			fsL["camera_matrix"] >> cameraMatrixLoaded;
			fsL["distortion_coefficients"] >> distCoeffsLoaded;
			fsL.release();


			if (cap.isOpened())  // get frame from camera
			{
				int c = 0;

				namedWindow("Original view", 1);
				for (;;)
				{
					c = waitKey(15);

					// get a new frame from camera
					Mat frame, imgclone, imgUndistort;

					cap >> frame;

					//display the frame 
					imshow("Original view", frame);

					imgclone = frame.clone();

					//Undistort
					undistort(imgclone, imgUndistort, cameraMatrix, distCoeffs);

					imshow("Undistorted view", imgUndistort);

					if (c == ESC_KEY) break;
				}
			}


			//close all windows
			destroyAllWindows();
		}
	}
	else if (flag == 2) {

		cameraMatrix = Mat::eye(3, 3, CV_64F);
		distCoeffs = Mat::zeros(8, 1, CV_64F);
		FileStorage fs("cameraparams.xml", FileStorage::READ);
		fs["camera_matrix"] >> cameraMatrix;
		fs["distortion_coefficients"] >> distCoeffs;
		fs.release();
	}
	else {
		cout << "Krivi unos!!" << endl;
		return -1;
	}



	Mat img;
	int new_or_load;

	cout << "Pritisni 1 za uzimanje nove slike ili 2 za ucitavanje postojece slike : ";
	cin >> new_or_load;

	if (new_or_load == 1) {
		if (cap.isOpened()) {
			int c = 0;
			namedWindow("Original view", 1);
			for (;;) {
				c = waitKey(15);


				Mat frame, imgclone, imgUndistort;
				cap >> frame;


				imshow("Original view", frame);

				Mat img_temp;
				cap >> img_temp;
				imwrite("../imgg.jpg", img_temp);
				cvtColor(img_temp, img, CV_RGB2GRAY);

				if (c == 'p') break;

			}
		}
	}
	else if (new_or_load == 2) {
		img = imread("../imgg.jpg", IMREAD_GRAYSCALE);
	}
	else {
		cout << "Krivi unos!" << endl;
		return -1;
	}

	namedWindow("Image taken");
	imshow("Image taken", img);

	User_data user_data;



	user_data.obj_pts.at<float>(0, 0) = 0.0f;
	user_data.obj_pts.at<float>(0, 1) = 0.0f;
	user_data.obj_pts.at<float>(0, 2) = 0.0f;

	user_data.obj_pts.at<float>(1, 0) = 250.0f;
	user_data.obj_pts.at<float>(1, 1) = 0.0f;
	user_data.obj_pts.at<float>(1, 2) = 0.0f;

	user_data.obj_pts.at<float>(2, 0) = 0.0f;
	user_data.obj_pts.at<float>(2, 1) = 173.0f;
	user_data.obj_pts.at<float>(2, 2) = 0.0f;

	user_data.obj_pts.at<float>(3, 0) = 250.0f;
	user_data.obj_pts.at<float>(3, 1) = 173.0f;
	user_data.obj_pts.at<float>(3, 2) = 0.0f;


	setMouseCallback("Image taken", Mouse_click, &user_data);



	for (;;) {
		int c = waitKey(15);
		if (user_data.num_clicks == 4) {
			setMouseCallback("Image taken", NULL, NULL);
			break;
		}
	}



	float cropped_width = user_data.img_pts.at<float>(1, 0) - user_data.img_pts.at<float>(0, 0);
	float cropped_height = user_data.img_pts.at<float>(2, 1) - user_data.img_pts.at<float>(0, 1);
	float u0 = user_data.img_pts.at<float>(0, 0);
	float v0 = user_data.img_pts.at<float>(0, 1);



	Rect rectROI(u0, v0, cropped_width, cropped_height);
	Mat img_roi = img(rectROI);
	imshow("ROI image", img_roi);
	cvtColor(img_roi, img_roi, COLOR_GRAY2BGR);



	Mat dst, color_dst;
	Canny(img_roi, dst, 40, 100, 3);
	cvtColor(dst, color_dst, COLOR_GRAY2BGR);
	imshow("Canny", dst);



	vector<Vec2f> lines;
	HoughLines(dst, lines, 1, CV_PI / 180, 50, 0, 0);

	if (lines.size() > 0) {


		float rho = lines[0][0];
		float theta = lines[0][1];

		float a = cos(theta);
		float b = sin(theta);

		float x0 = a * rho;
		float y0 = b * rho;

		Point pt1, pt2;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));

		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));

		line(color_dst, pt1, pt2, Scalar(0, 0, 255), 3, 16);
		line(img_roi, pt1, pt2, Scalar(0, 0, 255), 3, 16);

		imshow("Color Canny", color_dst);
		rho = rho + u0 * cos(theta) + v0 * sin(theta);


		Mat rotation_vector, translation_vector;
		solvePnP(user_data.obj_pts, user_data.img_pts, cameraMatrix, distCoeffs, rotation_vector, translation_vector);



		Mat R;
		Rodrigues(rotation_vector, R);


		Mat A;
		gemm(cameraMatrix, R, 1, 0, 0, A);


		Mat bb;
		gemm(cameraMatrix, translation_vector, 1, 0, 0, bb);



		float lambda_x = A.at<double>(0, 0) * cos(theta) + A.at<double>(1, 0) * sin(theta) - rho * A.at<double>(2, 0);
		float lambda_y = A.at<double>(0, 1) * cos(theta) + A.at<double>(1, 1) * sin(theta) - rho * A.at<double>(2, 1);
		float lambda_rho = bb.at<double>(2, 0) * rho - bb.at<double>(0, 0) * cos(theta) - bb.at<double>(1, 0) * sin(theta);

		float theta_n = atan2(lambda_y, lambda_x);
		float rho_n = lambda_rho / sqrt(lambda_x * lambda_x + lambda_y * lambda_y);


		string theta_img = "theta_n: " + to_string(theta_n * 180 / CV_PI);
		string rho_img = "rho_n: " + to_string(rho_n) + "mm";

		putText(img_roi, theta_img, Point(img_roi.cols - 300, img_roi.rows - 50), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0), 1, 16);
		putText(img_roi, rho_img, Point(img_roi.cols - 300, img_roi.rows - 30), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 0, 0), 1, 16);

		imshow("ROI image", img_roi);
	}
	else {
		cout << "nema detektiranih linija" << endl;
		return -1;
	}

	waitKey();

	return 0;
}

static void Mouse_click(int event, int x, int y, int flags, void* param) {
	if (event == CV_EVENT_LBUTTONDOWN) {
		User_data* user_data = (User_data*)param;

		user_data->img_pts.at<float>(user_data->num_clicks, 0) = x;
		user_data->img_pts.at<float>(user_data->num_clicks, 1) = y;

		user_data->num_clicks++;
	}
}


