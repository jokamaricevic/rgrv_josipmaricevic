#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <time.h>

using namespace std;
using namespace cv;

int sirina_prozora = 440;
int visina_prozora = 440;
int pomak_ruba = 20;

Mat slika;
Mat kopija_slike;

void bojanje_trokuta(Point2f prva_tocka, Point2f druga_tocka, Point2f treca_tocka);

void bojanje_susjednog(int prvi_vrh_id, int drugi_vrh_id, int treci_vrh_id, Subdiv2D* subdiv);

bool provjera_tocke(Point T, int vrh_id, int sljedeci_vrh_id, Subdiv2D* subdiv);

static void pritisak_tipke(int dogadaj, int x, int y, int flags, void* param);

void crtanje_delaunay(vector<int> vrhovi_lista_ids, Subdiv2D* subdiv);

void dohvat_vrhova(Subdiv2D* subdiv, vector<int>* vrhovi_lista_ids);

void umetanje_tocaka(Subdiv2D* subdiv, Rect* rect);



int main()
{
	srand(unsigned(time(NULL)));

	slika.create(sirina_prozora, visina_prozora, CV_8UC3);
	slika.setTo(255);

	Rect rect(0, 0, sirina_prozora, visina_prozora);
	Subdiv2D subdiv(rect);
	umetanje_tocaka(&subdiv, &rect);

	vector<int> vrhovi_lista_ids;
	dohvat_vrhova(&subdiv, &vrhovi_lista_ids);

	crtanje_delaunay(vrhovi_lista_ids, &subdiv);

	namedWindow("LV2");
	imshow("LV2", slika);

	setMouseCallback("LV2", pritisak_tipke, (void*)(&subdiv));
	waitKey(0);

	return 0;

}


void umetanje_tocaka(Subdiv2D* subdiv, Rect* rect) {
	subdiv->insert(Point2f(20, 20));
	subdiv->insert(Point2f(20, 420));
	subdiv->insert(Point2f(420, 20));
	subdiv->insert(Point2f(420, 420));

	for (int i = 0; i < 16; i++) {
		subdiv->insert(Point2f((int)(rand() % 
			(rect->width - pomak_ruba * 2) + pomak_ruba), 
			(int)(rand() % (rect->height - pomak_ruba * 2) + pomak_ruba)));
	}
}

void dohvat_vrhova(Subdiv2D* subdiv, vector<int>* vrhovi_lista_ids) {
	for (int i = 4; i < 24; i++) {
		int prvi_vrh;
		subdiv->getVertex(i, &prvi_vrh);
		vrhovi_lista_ids->push_back(prvi_vrh);

		int vrh_temp = prvi_vrh;

		while (1) {
			int sljedeci_vrh = subdiv->getEdge(vrh_temp, Subdiv2D::NEXT_AROUND_ORG);
			if (prvi_vrh == sljedeci_vrh) {
				break;
			}
			else {
				vrhovi_lista_ids->push_back(sljedeci_vrh);
				vrh_temp = sljedeci_vrh;
			}
		}
		cout << (i - 3) << ". " << subdiv->getVertex(i) << endl;
	}
	cout << "broj vrhova u listi: " << vrhovi_lista_ids->size() << endl;
}


void crtanje_delaunay(vector<int> vrhovi_lista_ids, Subdiv2D* subdiv) {
	for (auto i = vrhovi_lista_ids.begin(); i != vrhovi_lista_ids.end(); i++) {
		Point2f tocka_dst, tocka_org;

		if (subdiv->edgeDst(*i, &tocka_dst) > subdiv->edgeOrg(*i, &tocka_org)) {
			line(slika, tocka_org, tocka_dst, Scalar(0, 0, 0), 1, CV_AA, 0);
		}
	}
}

static void pritisak_tipke(int dogadaj, int x, int y, int flags, void* param) {
	if (dogadaj == CV_EVENT_LBUTTONDOWN) {
		Point pritisnuta_tocka(x, y);
		Subdiv2D* subdiv = (Subdiv2D*)param;

		if (pritisnuta_tocka.x >= 20 && pritisnuta_tocka.x <= 420 && pritisnuta_tocka.y >= 20 && pritisnuta_tocka.y <= 420) {
			int najbliza_tocka = subdiv->findNearest(pritisnuta_tocka);
			int prvi_vrh;
			subdiv->getVertex(najbliza_tocka, &prvi_vrh);

			int vrh_temp = prvi_vrh;

			while (1) {
				int sljedeci_vrh_id = subdiv->getEdge(vrh_temp, Subdiv2D::NEXT_AROUND_ORG);
				if (provjera_tocke(pritisnuta_tocka, vrh_temp, sljedeci_vrh_id, subdiv)) {
					Point2f tocka1, tocka2, tocka3;
					subdiv->edgeOrg(vrh_temp, &tocka1);
					subdiv->edgeDst(vrh_temp, &tocka2);
					subdiv->edgeDst(sljedeci_vrh_id, &tocka3);

					kopija_slike = slika.clone();

					bojanje_susjednog(subdiv->rotateEdge(vrh_temp, 2), sljedeci_vrh_id, 
						subdiv->rotateEdge(subdiv->getEdge(vrh_temp, Subdiv2D::NEXT_AROUND_LEFT), 2), subdiv);
					bojanje_trokuta(tocka1, tocka2, tocka3);
					imshow("LV2", kopija_slike);
					break;
				}
				else {
					vrh_temp = sljedeci_vrh_id;
					if (vrh_temp == prvi_vrh) {
						break;
					}
				}
			}
		}
	}
}

bool provjera_tocke(Point T, int vrh_id, int sljedeci_vrh_id, Subdiv2D* subdiv) {
	Point2f A, B, C;
	subdiv->edgeOrg(vrh_id, &A);
	subdiv->edgeDst(vrh_id, &B);
	subdiv->edgeDst(sljedeci_vrh_id, &C);

	bool A_unutar = A.x >= 20 && A.x <= 420 && A.y >= 20 && A.y <= 420;
	bool B_unutar = B.x >= 20 && B.x <= 420 && B.y >= 20 && B.y <= 420;
	bool C_unutar = C.x >= 20 && C.x <= 420 && C.y >= 20 && C.y <= 420;
	bool T_unutar = T.x >= 20 && T.x <= 420 && T.y >= 20 && T.y <= 420;

	if (T_unutar) {
		if (A_unutar && B_unutar && C_unutar) {
			float mi = ((B.x - A.x) * (T.y - A.y) - (B.y - A.y) * (T.x - A.x)) / ((C.y - A.y) * (B.x - A.x) - (B.y - A.y) * (C.x - A.x));
			float lambda = (T.x - A.x - mi * (C.x - A.x)) / (B.x - A.x);
			if (lambda >= 0.0 && mi >= 0.0 && (lambda + mi) <= 1.0) {
				return true;
			}
		}
	}
	return false;
}

void bojanje_susjednog(int prvi_vrh_id, int drugi_vrh_id, int treci_vrh_id, Subdiv2D* subdiv) {
	Point2f prvi_sljedeci_lijevi, drugi_slijedeci_lijevi, treci_slijedeci_lijevi;

	subdiv->edgeDst(subdiv->getEdge(prvi_vrh_id, Subdiv2D::NEXT_AROUND_LEFT), &prvi_sljedeci_lijevi);
	subdiv->edgeDst(subdiv->getEdge(drugi_vrh_id, Subdiv2D::NEXT_AROUND_LEFT), &drugi_slijedeci_lijevi);
	subdiv->edgeDst(subdiv->getEdge(treci_vrh_id, Subdiv2D::NEXT_AROUND_LEFT), &treci_slijedeci_lijevi);

	if (prvi_sljedeci_lijevi.x >= 20 && prvi_sljedeci_lijevi.x <= 420 && prvi_sljedeci_lijevi.y >= 20 && prvi_sljedeci_lijevi.y <= 420) {
		Point2f tocka1, tocka2;
		subdiv->edgeOrg(prvi_vrh_id, &tocka1);
		subdiv->edgeDst(prvi_vrh_id, &tocka2);
		vector<Point> pts;
		pts.push_back(tocka1);
		pts.push_back(tocka2);
		pts.push_back(prvi_sljedeci_lijevi);
		fillConvexPoly(kopija_slike, pts, Scalar(255, 0, 0), CV_AA, 0);
	}
	if (drugi_slijedeci_lijevi.x >= 20 && drugi_slijedeci_lijevi.x <= 420 && drugi_slijedeci_lijevi.y >= 20 && drugi_slijedeci_lijevi.y <= 420) {
		Point2f tocka1, tocka2;
		subdiv->edgeOrg(drugi_vrh_id, &tocka1);
		subdiv->edgeDst(drugi_vrh_id, &tocka2);
		vector<Point> pts;
		pts.push_back(tocka1);
		pts.push_back(tocka2);
		pts.push_back(drugi_slijedeci_lijevi);
		fillConvexPoly(kopija_slike, pts, Scalar(255, 0, 0), CV_AA, 0);
	}
	if (treci_slijedeci_lijevi.x >= 20 && treci_slijedeci_lijevi.x <= 420 && treci_slijedeci_lijevi.y >= 20 && treci_slijedeci_lijevi.y <= 420) {
		Point2f tocka1, tocka2;
		subdiv->edgeOrg(treci_vrh_id, &tocka1);
		subdiv->edgeDst(treci_vrh_id, &tocka2);
		vector<Point> pts;
		pts.push_back(tocka1);
		pts.push_back(tocka2);
		pts.push_back(treci_slijedeci_lijevi);
		fillConvexPoly(kopija_slike, pts, Scalar(255, 0, 0), CV_AA, 0);
	}

}

void bojanje_trokuta(Point2f prvi_vrh, Point2f drugi_vrh, Point2f treci_vrh) {
	vector<Point> pts;
	pts.push_back(prvi_vrh);
	pts.push_back(drugi_vrh);
	pts.push_back(treci_vrh);

	fillConvexPoly(kopija_slike, pts, Scalar(0, 0, 255), CV_AA, 0);
}
