// LV6.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>
#include <string>
#include <filesystem>
#include <opencv2/highgui.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core.hpp>
#include <chrono>


using namespace std;
using namespace cv;
using std::filesystem::current_path;

struct RV3DPOINT
{
	int u, v, d;
	int num_of_plane = -1;
};

void ReadKinectPic(string pathRGB, Mat* depthImage, vector<RV3DPOINT>* point3DArray, int& n3DPoints);
bool CalcPlaneCoeffs(RV3DPOINT& point3d_1, RV3DPOINT& point3d_2, RV3DPOINT& point3d_3, double& a, double& b, double& c);
void colorPlanes(Mat* image, vector<RV3DPOINT>* points, int num_of_planes);

int main()
{
	srand((unsigned)time(NULL));

	string path_image;

	string imageNum = "00242";
	cout << "Trenutno se koristi slika: " << imageNum << "\n";

	path_image = "./KinectPics/sl-" + imageNum + ".bmp";
	Mat rgb_image, depth_image;

	rgb_image = imread(path_image);

	
	cout << "Current working directory: " << current_path() << "\n";

	if (!rgb_image.data) return -1;
	imshow("RGB Image", rgb_image);
	waitKey(0);

	depth_image = rgb_image.clone();
	cvtColor(depth_image, depth_image, COLOR_BGR2GRAY);
	int num_3D_points;
	vector<RV3DPOINT> point_3d_array;

	ReadKinectPic(path_image, &depth_image, &point_3d_array, num_3D_points);
	cvtColor(depth_image, depth_image, CV_GRAY2BGR);

	imshow("Depth Image", depth_image);
	waitKey(0);

	/*******RANSAC*******/
	chrono::steady_clock::time_point begin = chrono::steady_clock::now();

	cout << "Starting RANSAC for planes..." << endl;
	double e = 4.0;

	vector<int> indexes_for_plane;
	vector<int> indexes_for_plane_best;

	int counter_W_current = 0;
	int counter_W_best = 0;
	int counter_plane_pts = 0;

	int plane_counter = 0;

	cout << "Points in point_3d_array: " << num_3D_points << endl;

	int first_pt_idx;
	int second_pt_idx;
	int third_pt_idx;
	double a, b, c;
	RV3DPOINT k_point;
	double distance;
	vector<int>::iterator indexes_plane_iter;


	//first loop for planes
	while (counter_plane_pts < 0.9 * num_3D_points)

	{
		indexes_for_plane_best.clear();
		counter_W_best = 0;


		for (int j = 0; j < 1000; j++)
		{
			counter_W_current = 0;
			indexes_for_plane.clear();

			do {
				first_pt_idx = rand() % (num_3D_points);
			} while (point_3d_array.at(first_pt_idx).num_of_plane != -1);

			do {
				second_pt_idx = rand() % (num_3D_points);
			} while (point_3d_array.at(second_pt_idx).num_of_plane != -1);

			do {
				third_pt_idx = rand() % (num_3D_points);
			} while (point_3d_array.at(third_pt_idx).num_of_plane != -1);

			//PLANE EQUATION
			if (CalcPlaneCoeffs(point_3d_array.at(first_pt_idx),
				point_3d_array.at(second_pt_idx),
				point_3d_array.at(third_pt_idx),
				a, b, c))
			{

				//going through vector to get "votes"
				for (int k = 0; k < num_3D_points; k++)
				{
					k_point = point_3d_array.at(k);

					if (k_point.num_of_plane == -1)
					{
						//calculating distance from point to plane
						distance = abs(k_point.d - (a * k_point.u + b * k_point.v + c));

						if (distance <= e)
						{
							counter_W_current++;
							indexes_for_plane.push_back(k);
						}
					}
				}
			}
			if (counter_W_current > counter_W_best)
			{
				counter_W_best = counter_W_current;
				indexes_for_plane_best.clear();
				indexes_for_plane_best = indexes_for_plane;
			}
		} //end of second "for" loop

		counter_plane_pts += indexes_for_plane_best.size();
		cout << "Counter: " << counter_plane_pts << " -- 90% of points: " << 0.9 * num_3D_points << endl;


		for (indexes_plane_iter = indexes_for_plane_best.begin();
			indexes_plane_iter != indexes_for_plane_best.end();
			indexes_plane_iter++)
		{
			point_3d_array[*indexes_plane_iter].num_of_plane = plane_counter;
		}

		cout << "Found coeffs for plane " << plane_counter + 1 << "..." << endl;

		plane_counter++;
	}

	chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	cout << "Time elapsed: " <<
		chrono::duration_cast<chrono::minutes>(end - begin).count() << "[min]" << endl;

	colorPlanes(&depth_image, &point_3d_array, plane_counter);
	imshow("Planes on image", depth_image);
	waitKey(0);

	return 0;
}

void colorPlanes(Mat* image, vector<RV3DPOINT>* points, int num_of_planes)
{
	for (int i = 0; i < num_of_planes; i++) {
		unsigned char blue = rand() % 255;
		unsigned char green = rand() % 255;
		unsigned char red = rand() % 255;

		for (int j = 0; j < points->size(); j++)
		{
			unsigned char* p = image->ptr(points->at(j).v, points->at(j).u);
			if (points->at(j).num_of_plane == 0) {
				p[0] = 0;		//B
				p[1] = 255;		//G
				p[2] = 0;		//R
			}
			else if (points->at(j).num_of_plane == i &&
				points->at(j).num_of_plane != 0) {
				p[0] = blue;	//B
				p[1] = green;	//G
				p[2] = red;		//R
			}
		}
	}
}

void ReadKinectPic(string pathRGB, Mat* depthImage, vector<RV3DPOINT>* point3DArray, int& n3DPoints)
{

	int* DepthMap = new int[depthImage->cols * depthImage->rows];
	memset(DepthMap, 0, depthImage->cols * depthImage->rows * sizeof(int));


	int u, v, d;
	int dmin = 2047;
	int dmax = 0;

	n3DPoints = 0;

	//Get DepthImage file path
	string pathDepth = pathRGB.substr(0, pathRGB.length() - 4);
	pathDepth.append("-D.txt");

	FILE* fp;

	fopen_s(&fp, pathDepth.c_str(), "r");

	if (fp)
	{
		bool bOK = true;

		//Determine max and min depth values and get Depth map
		for (v = 0; v < depthImage->rows; v++)
		{
			for (u = 0; u < depthImage->cols; u++)
			{
				if (!(bOK = (fscanf_s(fp, "%d ", &d) == 1)))
					break;


				if (d == 2047)
				{
					d = -1;
				}
				else
				{


					//determine min and max d
					if (d < dmin)
						dmin = d;

					if (d > dmax)
						dmax = d;
				}

				DepthMap[v * depthImage->cols + u] = d;


				if (d != -1)
				{
					RV3DPOINT pt3;
					pt3.u = u;
					pt3.v = v;
					pt3.d = d;

					point3DArray->push_back(pt3);

				}


			}
		}

		fclose(fp);
	}

	//get  number of valid 3D points
	n3DPoints = point3DArray->size();

	//Form grayscale pic -> Scale from 1 to 255 (reserve 0 for undefined regions)
	for (v = 0; v < depthImage->rows; v++)
	{
		for (u = 0; u < depthImage->cols; u++)
		{
			d = DepthMap[v * depthImage->cols + u];

			if (d != -1)
				d = ((d - dmin) * 254 / (dmax - dmin)) + 1;
			else
				d = 0;

			((uchar*)(depthImage->data + v * depthImage->step))[u] = d;

		}
	}

	delete[] DepthMap;
}

bool CalcPlaneCoeffs(RV3DPOINT& point3d_1, RV3DPOINT& point3d_2, RV3DPOINT& point3d_3,
	double& a, double& b, double& c)
{
	Mat A = Mat::zeros(3, 3, CV_64FC1);
	Mat Z = Mat::zeros(3, 1, CV_64FC1);
	Mat P = Mat::zeros(3, 1, CV_64FC1);

	A.at<double>(0, 0) = point3d_1.u;
	A.at<double>(0, 1) = point3d_1.v;
	A.at<double>(0, 2) = 1;

	A.at<double>(1, 0) = point3d_2.u;
	A.at<double>(1, 1) = point3d_2.v;
	A.at<double>(1, 2) = 1;

	A.at<double>(2, 0) = point3d_3.u;
	A.at<double>(2, 1) = point3d_3.v;
	A.at<double>(2, 2) = 1;

	Z.at<double>(0, 0) = point3d_1.d;
	Z.at<double>(1, 0) = point3d_2.d;
	Z.at<double>(2, 0) = point3d_3.d;

	if (solve(A, Z, P)) {
		a = P.at<double>(0, 0);
		b = P.at<double>(1, 0);
		c = P.at<double>(2, 0);
		return true;
	}
	return false;
}