﻿// LV7.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>

#include <conio.h>
#include <vtk-9.0/vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);
#include <vtk-9.0/vtkPolyData.h>
#include <vtk-9.0/vtkSmartPointer.h>
#include <vtk-9.0/vtkPolyDataMapper.h>
#include <vtk-9.0/vtkProperty.h>
#include <vtk-9.0/vtkActor.h>
#include <vtk-9.0/vtkRenderWindow.h>
#include <vtk-9.0/vtkRenderer.h>
#include <vtk-9.0/vtkRenderWindowInteractor.h>
#include <vtk-9.0/vtkInteractorStyle.h>
#include <vtk-9.0/vtkInteractorStyleTrackballCamera.h>
#include <vtk-9.0/vtkAxesActor.h>
#include <vtk-9.0/vtkTextActor.h>
#include <vtk-9.0/vtkTransform.h>
#include <vtk-9.0/vtkPLYReader.h>
#include <vtk-9.0/vtkIterativeClosestPointTransform.h>
#include <vtk-9.0/vtkLandmarkTransform.h>
#include <vtk-9.0/vtkTransformPolyDataFilter.h>

#include <Windows.h>
#include <ctime>
#include <chrono>
#include <time.h>



using namespace std;

int main()
{
	string pathDestination("../3DModels/bunny.ply");
	string pathSource("../3DModels/bunny_t1.ply");

	int iterations[5] = { 10, 50, 100, 500, 1000 };
	int landmarks[5] = { 10, 50, 100, 500, 1000 };
	ofstream results;
	results.open("results.csv");

	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			//Render 1
			vtkSmartPointer<vtkRenderer> renderer1 = vtkSmartPointer<vtkRenderer>::New();
			renderer1->SetBackground(0.5, 0.5, 0.5);

			//window1
			vtkSmartPointer<vtkRenderWindow> window1 = vtkSmartPointer<vtkRenderWindow>::New();
			window1->AddRenderer(renderer1);
			window1->SetSize(800, 600);
			window1->SetWindowName("Original position");

			//render window interactor - for mouse/key/time events
			vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor1 = vtkSmartPointer<vtkRenderWindowInteractor>::New();
			renderWindowInteractor1->SetRenderWindow(window1);
			/*************************************************************************/
			//Render 2
			vtkSmartPointer<vtkRenderer> renderer2 = vtkSmartPointer<vtkRenderer>::New();
			renderer2->SetBackground(0.5, 0.5, 0.5);

			// window2
			vtkSmartPointer<vtkRenderWindow> window2 = vtkSmartPointer<vtkRenderWindow>::New();
			window2->AddRenderer(renderer2);
			window2->SetSize(800, 600);
			window2->SetWindowName("Transformed position");

			//render window interactor - for mouse/key/time events
			vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor2 = vtkSmartPointer<vtkRenderWindowInteractor>::New();
			renderWindowInteractor2->SetRenderWindow(window2);


			/*************************************************************************/

			//ucitavanje prvog modela
			vtkSmartPointer<vtkPolyData> source;
			vtkSmartPointer<vtkPLYReader> plyReaderSrc = vtkSmartPointer<vtkPLYReader>::New();
			plyReaderSrc->SetFileName(pathSource.c_str());
			plyReaderSrc->Update();
			source = plyReaderSrc->GetOutput();

			//ucitavanje drugog modela
			vtkSmartPointer<vtkPolyData> destination;
			vtkSmartPointer<vtkPLYReader> plyReaderDst = vtkSmartPointer<vtkPLYReader>::New();
			plyReaderDst->SetFileName(pathDestination.c_str());
			plyReaderDst->Update();
			destination = plyReaderDst->GetOutput();

			/*************************************************************************/

			//iscrtavanje grafičkih primitiva - source
			vtkSmartPointer<vtkPolyDataMapper> mapperSrc = vtkSmartPointer<vtkPolyDataMapper>::New();
			mapperSrc->SetInputData(source);
			vtkSmartPointer<vtkActor> actSrc = vtkSmartPointer<vtkActor>::New();
			actSrc->SetMapper(mapperSrc);
			actSrc->GetMapper()->ScalarVisibilityOff();
			actSrc->GetProperty()->SetColor(0, 0, 1);
			renderer1->AddActor(actSrc);

			//iscrtavanje grafičkih primitiva - destination
			vtkSmartPointer<vtkPolyDataMapper> mapperDst = vtkSmartPointer<vtkPolyDataMapper>::New();
			mapperDst->SetInputData(destination);
			vtkSmartPointer<vtkActor> actDst = vtkSmartPointer<vtkActor>::New();
			actDst->SetMapper(mapperDst);
			actDst->GetMapper()->ScalarVisibilityOff();
			actDst->GetProperty()->SetColor(0, 1, 0);
			renderer1->AddActor(actDst);
			renderer2->AddActor(actDst);

			/*************************************************************************/
			//Implementacija iterativnog algoritma 
			vtkSmartPointer<vtkIterativeClosestPointTransform> ICP = vtkSmartPointer<vtkIterativeClosestPointTransform>::New();
			ICP->SetSource(source);
			ICP->SetTarget(destination);
			ICP->GetLandmarkTransform()->SetModeToRigidBody();
			ICP->SetMaximumNumberOfIterations(iterations[i]);
			ICP->SetMaximumNumberOfLandmarks(landmarks[j]);


			chrono::steady_clock::time_point begin = chrono::steady_clock::now();

			//POKRETANJE ITERATIVNOG ALGORITMA
			ICP->Update();

			chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
			// Mjerenje vremena 

			cout << "Time elapsed: " << chrono::duration_cast<chrono::microseconds>(end - begin).count() / 1000.0 << "[ms]" << endl;
			double timeMS = chrono::duration_cast<chrono::microseconds>(end - begin).count() / 1000.0;


			/*************************************************************************/

			vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
			transformFilter->SetInputData(source);
			transformFilter->SetTransform(ICP);
			transformFilter->Update();

			vtkSmartPointer<vtkPolyDataMapper> mapperTransform = vtkSmartPointer<vtkPolyDataMapper>::New();
			mapperTransform->SetInputConnection(transformFilter->GetOutputPort());

			vtkSmartPointer<vtkActor> actTransformed = vtkSmartPointer<vtkActor>::New();
			actTransformed->SetMapper(mapperTransform);
			actTransformed->GetProperty()->SetColor(0, 0, 1);
			renderer2->AddActor(actTransformed);


			/*************************************************************************/
			vtkSmartPointer<vtkDataArray> transformData;
			vtkSmartPointer<vtkDataArray> destinationData;

			transformData = transformFilter->GetOutput()->GetPoints()->GetData();
			destinationData = destination->GetPoints()->GetData();

			double sum = 0;
			double x1, x2, y1, y2, z1, z2, distance_squared;

			for (int tupleIdx = 0; tupleIdx < transformData->GetNumberOfTuples(); tupleIdx++) {
				x1 = transformData->GetTuple3(tupleIdx)[0];
				y1 = transformData->GetTuple3(tupleIdx)[1];
				z1 = transformData->GetTuple3(tupleIdx)[2];
				x2 = destinationData->GetTuple3(tupleIdx)[0];
				y2 = destinationData->GetTuple3(tupleIdx)[1];
				z2 = destinationData->GetTuple3(tupleIdx)[2];

				distance_squared = pow(x1 - x2, 2) + pow(y1 - y2, 2) + pow(z1 - z2, 2);

				sum += distance_squared;
			}

			double MSE = sum / (double)transformData->GetNumberOfTuples();
			string filepath = "file.csv";

			FILE* fileLV7;
			fopen_s(&fileLV7, filepath.c_str(), "w");
			fprintf(fileLV7, "%f, %f, %f \n", iterations, landmarks, MSE);



			results << iterations[i] << "," << landmarks[j] << "," << MSE << "," << timeMS << "\n";


			//rendering both windows
			window1->Render();
			window2->Render();
			renderWindowInteractor1->Start();
			renderWindowInteractor2->Start();
		}
	}

	results.close();
}