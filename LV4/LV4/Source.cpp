#include "pch.h"
#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video/tracking.hpp>
#include "opencv2/calib3d/calib3d.hpp"


using namespace std;
using namespace cv;


int main() {

	string img1 = "../ImageT0.jpg";
	string img2 = "../ImageT2.jpg";


	Mat img_object, img_scene;
	img_object = imread(img1);
	img_scene = imread(img2);

	imshow("druga slika", img_scene);
	imshow("prva slika", img_object);

	Rect rect = selectROI("prva slika", img_object);
	Mat img_object_roi = img_object(rect);

	Ptr<Feature2D> detector = SIFT::create();

	vector<KeyPoint> keypts_object;
	Mat descriptor_object;
	detector->detectAndCompute(img_object_roi, noArray(), keypts_object, descriptor_object);

	vector<KeyPoint> keypts_scene;
	Mat descriptor_scene;
	detector->detectAndCompute(img_scene, noArray(), keypts_scene, descriptor_scene);

	Mat img_keypts_object, img_keypts_scene;
	drawKeypoints(img_object_roi, keypts_object, img_keypts_object, Scalar::all(-1));
	drawKeypoints(img_scene, keypts_scene, img_keypts_scene, Scalar::all(-1));

	imshow("kljucni objekt", img_keypts_object);
	imshow("kljucna scena", img_keypts_scene);
	waitKey(0);

	Ptr<DescriptorMatcher> matcher =
		DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
	vector<vector<DMatch>> knn_matches;
	matcher->knnMatch(descriptor_object, descriptor_scene, knn_matches, 2);

	const float ratio_thresh = 0.75f;
	vector<DMatch> good_matches;

	for (size_t i = 0; i < knn_matches.size(); i++) {

		if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance) {

			good_matches.push_back(knn_matches[i][0]);

		}

	}

	Mat img_matches;
	drawMatches(img_object_roi, keypts_object, img_scene, keypts_scene, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	vector<Point2f> object, scene;
	for (size_t i = 0; i < good_matches.size(); i++) {

		object.push_back(keypts_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypts_scene[good_matches[i].trainIdx].pt);

	}

	Mat H;

	try {

		H = findHomography(object, scene, RANSAC);

	}

	catch (Exception e) {

		cout << "nije moguce naci matricu homografije" << endl;
		return -1;

	}

	vector<Point2f> object_corners(4);
	object_corners[0] = Point2f(0.0, 0.0);
	object_corners[1] = Point2f(float(img_object_roi.cols), 0.0);
	object_corners[2] = Point2f(float(img_object_roi.cols), float(img_object_roi.rows));
	object_corners[3] = Point2f(0.0, float(img_object_roi.rows));

	vector<Point2f> scene_corners(4);

	try {

		perspectiveTransform(object_corners, scene_corners, H);
		line(img_matches, scene_corners[0] + object_corners[1], scene_corners[1] + object_corners[1], Scalar(255, 0, 0), 2);
		line(img_matches, scene_corners[1] + object_corners[1], scene_corners[2] + object_corners[1], Scalar(255, 0, 0), 2);
		line(img_matches, scene_corners[2] + object_corners[1], scene_corners[3] + object_corners[1], Scalar(255, 0, 0), 2);
		line(img_matches, scene_corners[3] + object_corners[1], scene_corners[0] + object_corners[1], Scalar(255, 0, 0), 2);

	}

	catch (Exception e) {

		cout << "\nne moze pronaci podudarnost" << endl;
		img_matches = Mat::zeros(Size(600, 400), CV_64FC1);
		putText(img_matches, "ne moze pronaci podudarnosti", Point(120, 200), FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255), 1);

	}

	imshow("dobra podudarnost", img_matches);
	waitKey();

	return 0;

}