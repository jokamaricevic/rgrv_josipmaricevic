﻿#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <time.h>
#include <fstream>

#define OPENCV_TRAITS_ENABLE_DEPRECATED

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video/tracking.hpp>
//#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>

using namespace std;
using namespace cv;
//using namespace xfeatures2d;

void getCameraParams(string fileName, Mat* cameraMatrix, Mat* distorsionCoefficients);
bool loadAndShowImages(string leftImageFilename, string rightImageFilename, Mat* leftImage, Mat* rightImage);
void detectAndShowSIFTfeatures(Mat* leftImage, Mat* rightImage, vector<KeyPoint>* leftImageKeypoints, 
								vector<KeyPoint>* rightImageKeypoints, vector<DMatch>* goodMatches);
void findFunamentalMatrix(Mat* fundamentalMatrix, vector<KeyPoint>* leftImageKeypoints,
								vector<KeyPoint>* rightImageKeypoints, vector<DMatch>* goodMatches,
								vector<Point2f>* leftImage2fPoints, vector<Point2f>* rightImage2fPoints, vector<uchar>* arrayMask);
void showCombinedImage(Mat* leftImage, Mat* rightImage, vector<KeyPoint>* leftImageKeypoints, vector<KeyPoint>* rightImageKeypoints,
					vector<Point2f>* leftImage2fPoints, vector<Point2f>* rightImage2fPoints, vector<uchar>* arrayMask);
void Convert2DPointsTo3DPoints(vector<KeyPoint>& points2D_L, vector<KeyPoint>& points2D_R, Mat& E, Mat& P, Mat& points3D);
void ransac3Planes(vector<Point3f>* points3Dv);
int main() {

	srand(unsigned(time(NULL)));

	string cameraParamsFilename = "cameraparams.xml";
	string leftImageFilename = "image3.bmp";
	string rightImageFilename = "image4.bmp";
	string points3dFilename = "points3d.txt";

	Mat cameraMatrix, distorsionCoefficients;
	Mat leftImage, rightImage;
	Mat fundamentalMatrix;
	vector<KeyPoint> leftImageKeypoints, rightImageKeypoints;
	vector<DMatch> goodMatches;
	vector<Point2f> leftImage2fPoints, rightImage2fPoints;
	vector<uchar> arrayMask;

	getCameraParams(cameraParamsFilename, &cameraMatrix, &distorsionCoefficients);
	if (!loadAndShowImages(leftImageFilename, rightImageFilename, &leftImage, &rightImage)) return -1;
	detectAndShowSIFTfeatures(&leftImage, &rightImage, &leftImageKeypoints, &rightImageKeypoints, &goodMatches);
	findFunamentalMatrix(&fundamentalMatrix, &leftImageKeypoints, &rightImageKeypoints, &goodMatches, &leftImage2fPoints, &rightImage2fPoints, &arrayMask);
	showCombinedImage(&leftImage, &rightImage, &leftImageKeypoints, &rightImageKeypoints, &leftImage2fPoints, &rightImage2fPoints, &arrayMask);
	fundamentalMatrix.convertTo(fundamentalMatrix, CV_32F);
	cameraMatrix.convertTo(cameraMatrix, CV_32F);
	Mat E;
	try {
		E = cameraMatrix.t() * fundamentalMatrix * cameraMatrix;
	}
	catch (exception e) {
		cout << "Couldn't calculate essential matrix." << endl;
		return -1;
	}

	Mat points3D = Mat(3, leftImageKeypoints.size(), CV_32F);
	Convert2DPointsTo3DPoints(leftImageKeypoints, rightImageKeypoints, E, cameraMatrix, points3D);

	FILE* pts3Dfile;
	fopen_s(&pts3Dfile, points3dFilename.c_str(), "w");
	vector<Point3f> points3Dv;

	for (int i = 0; i < points3D.cols; i++) {
		fprintf(pts3Dfile, "%f %f %f \n", points3D.at<float>(0, i),
			points3D.at<float>(1, i), points3D.at<float>(2, i));

		points3Dv.push_back(Point3f(points3D.at<float>(0, i), points3D.at<float>(1, i),
			points3D.at<float>(2, i)));
	}
	fclose(pts3Dfile);


	ransac3Planes(&points3Dv);

	return 0;
}

void getCameraParams(string fileName, Mat *cameraMatrix, Mat * distorsionCoefficients) {
	*cameraMatrix = Mat::eye(3, 3, CV_64F);
	*distorsionCoefficients = Mat::zeros(8, 1, CV_64F);
	FileStorage fileStorage("fileName", FileStorage::READ);
	fileStorage["camera_matrix"] >> *cameraMatrix;
	fileStorage["distorsion_coefficients"] >> *distorsionCoefficients;
	fileStorage.release();
}

bool loadAndShowImages(string leftImageFilename, string rightImageFilename, Mat* leftImage, Mat* rightImage) {
	*leftImage = imread(leftImageFilename);
	*rightImage = imread(rightImageFilename);
	if (!leftImage || !rightImage) return false;

	imshow("Left Image", *leftImage);
	imshow("Right Image", *rightImage);
	waitKey(0);
	return true;
}

void detectAndShowSIFTfeatures(Mat* leftImage, Mat* rightImage, vector<KeyPoint>* leftImageKeypoints, 
								vector<KeyPoint>*  rightImageKeypoints, vector<DMatch>* goodMatches) {
	Mat leftImageWithKeypoints, rightImageWithKeypoints;
	Mat leftDescriptor, rightDescriptor;
	Mat imageMatches;
	Ptr<Feature2D> detector = SIFT::create();

	detector->detectAndCompute(*leftImage, noArray(), *leftImageKeypoints, leftDescriptor);
	detector->detectAndCompute(*rightImage, noArray(), *rightImageKeypoints, rightDescriptor);
	drawKeypoints(*leftImage, *leftImageKeypoints, leftImageWithKeypoints, Scalar::all(-1));
	drawKeypoints(*rightImage, *rightImageKeypoints, rightImageWithKeypoints, Scalar::all(-1));

	imshow("Left image with keypoints", leftImageWithKeypoints);
	imshow("Right image with keypoints", rightImageWithKeypoints);
	waitKey(0);

	Ptr<DescriptorMatcher> matcher =
		DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
	vector<vector<DMatch>> knn_matches;
	matcher->knnMatch(leftDescriptor, rightDescriptor, knn_matches, 2);

	const float ratio_thresh = 0.75f;

	for (size_t i = 0; i < knn_matches.size(); i++) {
		if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance) {
			goodMatches->push_back(knn_matches[i][0]);
		}
	}

	drawMatches(*leftImage, *leftImageKeypoints, *rightImage, *rightImageKeypoints, *goodMatches,
		imageMatches, Scalar::all(-1), Scalar::all(-1), vector<char>(),
		DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	imshow("Matches", imageMatches);
	waitKey(0);
}

void findFunamentalMatrix(Mat* fundamentalMatrix, vector<KeyPoint>* leftImageKeypoints, 
				vector<KeyPoint>* rightImageKeypoints, vector<DMatch>* goodMatches, 
				vector<Point2f>* leftImage2fPoints, vector<Point2f>* rightImage2fPoints, vector<uchar>* arrayMask) {

	for (size_t i = 0; i < goodMatches->size(); i++) {
		leftImage2fPoints->push_back(leftImageKeypoints->at(goodMatches->at(i).queryIdx).pt);
		rightImage2fPoints->push_back(rightImageKeypoints->at(goodMatches->at(i).trainIdx).pt);
	}

	Mat mask;
	*fundamentalMatrix = findFundamentalMat(*leftImage2fPoints, *rightImage2fPoints,
		FM_RANSAC, 3, 0.99, mask);

	if (mask.isContinuous()) {
		arrayMask->assign(mask.data, mask.data + mask.total());
	}
	else {
		for (int i = 0; i < mask.rows; i++) {
			arrayMask->insert(arrayMask->end(), mask.ptr<uchar>(i),
				mask.ptr<uchar>(i) + mask.cols);
		}
	}
}

void showCombinedImage(Mat* leftImage, Mat* rightImage, vector<KeyPoint>* leftImageKeypoints,
						vector<KeyPoint>* rightImageKeypoints, vector<Point2f>* leftImage2fPoints, 
						vector<Point2f>* rightImage2fPoints, vector<uchar>* arrayMask) {
	Mat combined_image;

	hconcat(*leftImage, *rightImage, combined_image);

	RNG rng(54621);
	for (int i = 0; i < arrayMask->size(); i++) {
		if (arrayMask->at(i) == 1) {
			leftImageKeypoints->push_back(KeyPoint(leftImage2fPoints->at(i), 1.f));
			rightImageKeypoints->push_back(KeyPoint(rightImage2fPoints->at(i), 1.f));


			Scalar color = Scalar(rng.uniform(0, 255));

			circle(combined_image, leftImage2fPoints->at(i), 3, color, 1, CV_AA);
			circle(combined_image, rightImage2fPoints->at(i) + Point2f(leftImage->cols, 0),
				3, color, 1, CV_AA);

			line(combined_image, leftImage2fPoints->at(i),
				rightImage2fPoints->at(i) + Point2f(leftImage->cols, 0), color,
				1, CV_AA, 0);
		}
	}

	imshow("Combined image", combined_image);
	waitKey(0);
}

void Convert2DPointsTo3DPoints(vector<KeyPoint>& points2D_L, vector<KeyPoint>& points2D_R, Mat& E, Mat& P, Mat& points3D)
{
	/*
	Details in:
	D. Kurtagić, Trodimenzionalna rekonstrukcija scene iz dvije slike (završni rad - preddiplomski
	studij). Elektrotehnički fakultet, Osijek, September 9, 2010.
	*/

	//Find the inverse of P (projection matrix)
	Mat Pinv;

	invert(P, Pinv, DECOMP_SVD);

	//Determine the singular value decomposition (svd) of E (essential matrix)
	Mat U, V, Vt, D;
	SVD::compute(E, D, U, Vt);

	//Define W
	Mat W = Mat::zeros(3, 3, CV_32F);
	W.at<float>(0, 1) = -1;
	W.at<float>(1, 0) = 1;
	W.at<float>(2, 2) = 1;

	Mat A = U * W * Vt;

	Mat b = Mat::zeros(3, 1, CV_32F);
	b.at<float>(0, 0) = U.at<float>(0, 2);
	b.at<float>(1, 0) = U.at<float>(1, 2);
	b.at<float>(2, 0) = U.at<float>(2, 2);

	Mat Ainv, Ainv_b;
	invert(A, Ainv, DECOMP_SVD);
	Ainv_b = Ainv * b;

	/**** Helper matrices ****/
	Mat Lpi = Mat(3, 1, CV_32F);
	Mat Rpi = Mat(3, 1, CV_32F);
	Mat ARpi = Mat(3, 1, CV_32F);

	Mat S = Mat(2, 1, CV_32F);

	Mat X = Mat(2, 2, CV_32F);
	Mat x1 = Mat(1, 1, CV_32F);
	Mat x2 = Mat(1, 1, CV_32F);
	Mat x4 = Mat(1, 1, CV_32F);

	Mat Y = Mat(2, 1, CV_32F);
	Mat y1 = Mat(1, 1, CV_32F);
	Mat y2 = Mat(1, 1, CV_32F);

	//2D points in left (model) and right (scene) images
	Mat Lm = Mat(3, 1, CV_32F);
	Mat Rm = Mat(3, 1, CV_32F);

	//Iteratively convert 2D point pairs to 3D points
	for (size_t i = 0; i < points2D_L.size(); i++)
	{
		Lm.at<float>(0, 0) = points2D_L[i].pt.x;
		Lm.at<float>(1, 0) = points2D_L[i].pt.y;
		Lm.at<float>(2, 0) = 1;

		Rm.at<float>(0, 0) = points2D_R[i].pt.x;
		Rm.at<float>(1, 0) = points2D_R[i].pt.y;
		Rm.at<float>(2, 0) = 1;

		Lpi = Pinv * Lm;
		Rpi = Pinv * Rm;

		//Init X table
		ARpi = Ainv * Rpi;
		x1 = Lpi.t() * Lpi;
		x2 = Lpi.t() * ARpi;
		x4 = ARpi.t() * ARpi;

		X.at<float>(0, 0) = -x1.at<float>(0, 0);
		X.at<float>(0, 1) = x2.at<float>(0, 0);
		X.at<float>(1, 0) = x2.at<float>(0, 0);
		X.at<float>(1, 1) = -x4.at<float>(0, 0);

		//Init Y table
		y1 = Lpi.t() * Ainv_b;
		y2 = ARpi.t() * Ainv_b;

		Y.at<float>(0, 0) = -y1.at<float>(0, 0);
		Y.at<float>(1, 0) = y2.at<float>(0, 0);

		solve(X, Y, S);

		float s = S.at<float>(0, 0);
		float t = S.at<float>(1, 0);

		Lpi.at<float>(0, 0) = s * Lpi.at<float>(0, 0);
		Lpi.at<float>(1, 0) = s * Lpi.at<float>(1, 0);
		Lpi.at<float>(2, 0) = s * Lpi.at<float>(2, 0);

		ARpi.at<float>(0, 0) = t * ARpi.at<float>(0, 0);
		ARpi.at<float>(1, 0) = t * ARpi.at<float>(1, 0);
		ARpi.at<float>(2, 0) = t * ARpi.at<float>(2, 0);

		points3D.at<float>(0, i) = (Lpi.at<float>(0, 0) + ARpi.at<float>(0, 0) - Ainv_b.at<float>(0, 0)) / 2;
		points3D.at<float>(1, i) = (Lpi.at<float>(1, 0) + ARpi.at<float>(1, 0) - Ainv_b.at<float>(1, 0)) / 2;
		points3D.at<float>(2, i) = (Lpi.at<float>(2, 0) + ARpi.at<float>(2, 0) - Ainv_b.at<float>(2, 0)) / 2;

	}


}

void ransac3Planes(vector<Point3f>* points3Dv) {
	cout << "Starting RANSAC for planes..." << endl;

	float e = 0.2;

	vector<Point3f> points3Dv_remaining = *points3Dv;
	vector<Point3f> points3Dv_remaining_best = *points3Dv;
	Mat planes = Mat::zeros(3, 4, CV_32F);

	int counter_W_current = 0;
	int counter_W_best = 0;

	float a, b, c, d;

	//3 loops for planes
	for (int i = 0; i < 3; i++) {

		//4000 iterations for every plane
		for (int j = 0; j < 4000; j++) {
			points3Dv_remaining = *points3Dv;
			counter_W_current = 0;


			int first_pt_idx = rand() % (points3Dv_remaining.size());
			int second_pt_idx = rand() % (points3Dv_remaining.size());
			int third_pt_idx = rand() % (points3Dv_remaining.size());

			//PLANE EQUATION
			//point P
			float x1 = points3Dv_remaining.at(first_pt_idx).x;
			float y1 = points3Dv_remaining.at(first_pt_idx).y;
			float z1 = points3Dv_remaining.at(first_pt_idx).z;

			//point Q
			float x2 = points3Dv_remaining.at(second_pt_idx).x;
			float y2 = points3Dv_remaining.at(second_pt_idx).y;
			float z2 = points3Dv_remaining.at(second_pt_idx).z;

			//point R
			float x3 = points3Dv_remaining.at(third_pt_idx).x;
			float y3 = points3Dv_remaining.at(third_pt_idx).y;
			float z3 = points3Dv_remaining.at(third_pt_idx).z;

			//vector PQ
			float a1 = x2 - x1;
			float b1 = y2 - y1;
			float c1 = z2 - z1;

			//vector PR
			float a2 = x3 - x1;
			float b2 = y3 - y1;
			float c2 = z3 - z1;

			//normal vector to the plane is PQ x PR
			//PQ X PR = (b1 * c2 - b2 * c1)*i +(a2 * c1 - a1 * c2)*j+ 
			//	(a1 * b2 - b1 * a2)*k = a*i + b*j + c*k
			a = b1 * c2 - b2 * c1;
			b = a2 * c1 - a1 * c2;
			c = a1 * b2 - b1 * a2;
			d = (-a * x1 - b * y1 - c * z1);

			//going through vector to get "votes"
			for (int k = 0; k < points3Dv_remaining.size(); k++) {
				float x_k = points3Dv_remaining.at(k).x;
				float y_k = points3Dv_remaining.at(k).y;
				float z_k = points3Dv_remaining.at(k).z;

				//calculating distance from point to plane
				double distance = abs((a * x_k + b * y_k + c * z_k + d) /
					(sqrt(a * a + b * b + c * c)));


				if (distance <= e) {
					counter_W_current++;

					points3Dv_remaining.erase(points3Dv_remaining.begin() + k);
				}

			}


			if (counter_W_current > counter_W_best) {
				points3Dv_remaining_best = points3Dv_remaining;
				counter_W_best = counter_W_current;
				planes.at<float>(i, 0) = a;
				planes.at<float>(i, 1) = b;
				planes.at<float>(i, 2) = c;
				planes.at<float>(i, 3) = d;

			}

		} //end of second for loop
		*points3Dv = points3Dv_remaining_best;
		counter_W_current = 0;
		counter_W_best = 0;

		cout << "Found coeffs for plane " << i + 1 << "..." << endl;
	} //end of first for loop


	cout << "Planes: " << planes << endl;
	cout << "Finding planes done. Storing to file..." << endl;

	//Store plane coeffs in file
	FILE* planes_file;
	fopen_s(&planes_file, "planes.txt", "w");

	for (int i = 0; i < 3; i++) {
		fprintf(planes_file, "%f %f %f %f \n", planes.at<float>(i, 0), planes.at<float>(i, 1),
			planes.at<float>(i, 2), planes.at<float>(i, 3));
	}
	fclose(planes_file);
}